const sql = require("./db.js");
var bcrypt = require('bcryptjs');
  
// constructor
const User = function(User) {
  this.fullName = User.fullName;
  this.email = User.email;
  this.password = User.password;
  this.status = User.status
};

User.create = (newUser, result) => {
  bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(newUser.password, salt, function(err, hash) {
          // Store hash in your password DB.
          newUser.password = hash;
          sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
            if (err) {
              console.log("error: ", err);
              result(err, null);
              return;
            }
            console.log("created User: ", { id: res.insertId, ...newUser });
            result(null, { id: res.insertId, ...newUser });
          })
      })
  })
};

User.verify = (email,password, result) => {
  sql.query('SELECT * FROM users WHERE email = "'+email+'"', (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }
    if (res.length) {
      bcrypt.compare(password, res[0].password, function(err, doesMatch){
        if(err){
          result(err, null);
          return;
        }else{
          console.log("found User: ", res[0]);
          result(null, res[0]);
          return;
        }
      })
    }else{
      //not found User
      result({ kind: "not_found" }, null);
    }   
  });
};

User.findById = (id, result) => {
  sql.query(`SELECT * FROM users WHERE id = ${id}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found User: ", res[0]);
      result(null, res[0]);
      return;
    }

    // not found User with the id
    result({ kind: "not_found" }, null);
  });
};

User.updateById = (id, User, result) => {
  sql.query(
    "UPDATE users SET fullName = ?, email = ?, password = ? WHERE id = ?",
    [User.fullName, User.email, User.password, id],
    (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }

      if (res.affectedRows == 0) {
        // not found User with the id
        result({ kind: "not_found" }, null);
        return;
      }

      console.log("updated User: ", { id: id, ...User });
      result(null, { id: id, ...User });
    }
  );
};


module.exports = User;
