# Angular11 and Node.js Rest APIs with Express & MySQL example
 Sample application for Admin app

 #mysql database setup:

```
 database required : testdb
 ```

 ```
 A table is required : users
 ```

 users table should have following properties:
 fullName, email, password, status
 
 ```
 import users.sql file to "testdb" database  
 ```
 ## user can login with following credentials:
 email: test@xyz.com
 password: admin123

## Project setup
```
npm install
```

```
cd frontend
```
```
npm install
```

```
npm run build
```
### Run
```
node server.js
```
