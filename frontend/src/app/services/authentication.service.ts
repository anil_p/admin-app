import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class AuthenticationService {
    baseUrl:string = 'http://localhost:3000';
    constructor(private http:HttpClient) {

    }

    loginUser(user) {
        let headers = new HttpHeaders();
        headers.append('Content-type', 'application/json');
        return this.http.post(this.baseUrl + '/api/users/login', user, {headers: headers});
    }

    logout() {
        localStorage.removeItem('user');
    }

    loggedIn() {
        var user = localStorage.getItem('user');
        if (user !== null) {
            return true;
        } else {
            return false;
        }
    }

    store(user){
        localStorage.setItem('user',JSON.stringify(user));
    }

}
