import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()

export class AuthGuard implements CanActivate{
    constructor(private authService: AuthenticationService, private router: Router) {

    }

    canActivate(route: ActivatedRouteSnapshot) {
        if(this.authService.loggedIn()) {
            return true;
        } else  {
            this.router.navigate(['']);
        }
    }
}
