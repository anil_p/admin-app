import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent{ 
  public email:string = '';
  public password:string = '';

  constructor(
    public authService: AuthenticationService,
    private router: Router
  ) { }
  
  login(){
    var userObj = {
      email: this.email,
      password: this.password
    }
    console.log(userObj)
    this.authService.loginUser(userObj).subscribe((data:any) => {
      this.authService.store(data);
      this.router.navigate(['/dashboard']);
    },err=>{
      alert("Error while login "+err)
      console.log("error while login...",err);
    })
  }
}
